//
//  ViewController.m
//  MulticolorLayerDemo
//
//  Created by Liuyu on 14-7-5.
//  Copyright (c) 2014年 Liuyu. All rights reserved.
//

#import "ViewController.h"
#import "MulticolorView.h"
#import "AppDelegate.h"
#import <CoreBluetooth/CoreBluetooth.h>
@interface ViewController ()
{
    
    __weak IBOutlet NSLayoutConstraint *heightConstraint;
    __weak IBOutlet UIView *centerView;
    
    CALayer *_layer;
    CAAnimationGroup *_animaTionGroup;
    CADisplayLink *_disPlayLink;
    MulticolorView *view;
    
    NSString *userData;
}

@property (weak, nonatomic) IBOutlet UITextField *tf_identification;
@property (weak, nonatomic) IBOutlet UITextField *tf_identificationiPad;




@end
@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  
    view = [[MulticolorView alloc] initWithFrame:centerView.frame];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [centerView addSubview:view];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    userData = delegate.userData;
    if (userData) {
        self.tf_identification.text = userData;
        self.tf_identificationiPad.text = userData;
        [self action_confirm:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];

}

-(void) dismissKeyboard:(id)sender{
    [self.view endEditing:true];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Animation
- (void)startAnimation
{
    CALayer *layer = [[CALayer alloc] init];
    layer.cornerRadius = [UIScreen mainScreen].bounds.size.width/2;
    layer.frame = CGRectMake(0, 0, layer.cornerRadius * 2, layer.cornerRadius * 2);
    layer.position = self.view.layer.position;
    UIColor *color = [UIColor colorWithRed:138.0/255.0 green:1.0 blue:1.0 alpha:1.0];
    layer.backgroundColor = color.CGColor;
    [self.view.layer addSublayer:layer];
    
    CAMediaTimingFunction *defaultCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    
    _animaTionGroup = [CAAnimationGroup animation];
    _animaTionGroup.delegate = self;
    _animaTionGroup.duration = 2;
    _animaTionGroup.removedOnCompletion = YES;
    _animaTionGroup.timingFunction = defaultCurve;
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
    scaleAnimation.fromValue = @0.0;
    scaleAnimation.toValue = @1.0;
    scaleAnimation.duration = 2;
    
    CAKeyframeAnimation *opencityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opencityAnimation.duration = 2;
    opencityAnimation.values = @[@0.8,@0.4,@0];
    opencityAnimation.keyTimes = @[@0,@0.5,@1];
    opencityAnimation.removedOnCompletion = YES;
    
    NSArray *animations = @[scaleAnimation,opencityAnimation];
    _animaTionGroup.animations = animations;
    [layer addAnimation:_animaTionGroup forKey:nil];
    
    [self performSelector:@selector(removeLayer:) withObject:layer afterDelay:1.5];
    [self.view bringSubviewToFront:centerView];
}

- (void)removeLayer:(CALayer *)layer
{
    [layer removeFromSuperlayer];
}

- (void)eventBlueLight
{
    if (!_disPlayLink) {
        _disPlayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(delayAnimation)];
        _disPlayLink.frameInterval = 120;
        [_disPlayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
}

- (void)delayAnimation
{
    [self startAnimation];
}

#pragma mark - Button
- (IBAction)action_kill:(id)sender {
    kill(getpid(), SIGKILL);

}
- (IBAction)action_killIpad:(id)sender {
    [self action_kill:sender];
}
- (IBAction)action_confirmIPad:(id)sender {
    [self action_confirm:sender];
}

- (IBAction)action_confirm:(id)sender {

    if ([self validate]) {
        [self eventBlueLight];
        [view startAnimation];
        
        self.tf_identification.userInteractionEnabled = false;
        self.tf_identificationiPad.userInteractionEnabled = false;

        
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            userData = self.tf_identificationiPad.text;
        }
        else{
            userData = self.tf_identification.text;
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:userData forKey:@"userkey"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [delegate startAdvertising];

    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"請輸入ID" delegate:nil cancelButtonTitle:@"確認" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

#pragma mark - Private method
- (BOOL) validate{
    if ((self.tf_identification.text == nil && self.tf_identificationiPad.text == nil) ||
        (self.tf_identification.text.length == 0 && self.tf_identificationiPad.text.length == 0) ) {
        return false;
    }
    return true;
}
- (void)keyboardFrameWillChange:(NSNotification *)notification
{
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardBeginFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    UIViewAnimationCurve animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    CGRect keyboardFrameEnd = [self.view convertRect:keyboardEndFrame toView:nil];
    CGRect keyboardFrameBegin = [self.view convertRect:keyboardBeginFrame toView:nil];
    
    newFrame.origin.y -= (keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}
@end
